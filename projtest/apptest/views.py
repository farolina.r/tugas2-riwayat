from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
import requests

RIWAYAT_API = "https://private-e52a5-ppw2017.apiary-mock.com/riwayat"

response = {'author':'Farol'}
def index(request):
	response['rahasia'] = False
	response['riwayats'] = get_riwayat()
	html = "apptest/riwayat.html"
	return render(request, html, response)
	
def get_riwayat():
	riwayats = requests.get(RIWAYAT_API)
	return riwayats.json()