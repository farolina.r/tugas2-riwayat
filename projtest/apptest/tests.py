from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class RiwayatUnitTest(TestCase):

    def test_riwayat_url_is_exist(self):
        response = Client().get('/apptest/')
        self.assertEqual(response.status_code, 200)

    def test_riwayat_using_index_func(self):
        found = resolve('/apptest/')
        self.assertEqual(found.func, index)
